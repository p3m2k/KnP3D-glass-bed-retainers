# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="KnP3D Glass Bed Retainers"
SLUG="knp3d_glass_bed_retainers"
DESCRIPTION="Retainers that hold a plate of glass in place on the bed of a Klic-N-Print 3-D printer."
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2023"
RELEASE_VERSION="1.0.0"
RELEASE_DATE="2023-02-13"
