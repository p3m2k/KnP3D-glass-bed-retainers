#########################
KnP3D Glass Bed Retainers
#########################


Introduction
============

This repo contains design files for parts that hold a plate of glass in
place on the bed of a Klic-N-Print 3D printer (KnP3D).  Included are a
retainer that fits over the corner of the bed, and a knob that holds a
standard hex nut, which secures the retainer to the bed while allowing
easy adjustment.

.. figure:: doc/images/retainer_assy.png
   :alt: Isometric CAD view of retainer parts.

   Isometric CAD view of retainer parts.

The parts are designed with FreeCAD_, a free (GPLv3) parametric 3D CAD
tool.

STL files generated from the design files can be found in the /build
directory.

.. figure:: doc/images/KnP3D_glass_bed_retainers.jpeg
   :alt: Photo of print bed with glass plate installed.

   Print bed with glass plate installed.

(The Klic-N-Print 3D is a dual-extruder, FDM 3D printer, very similar to
the Flash Forge Creator Pro.  It was discontinued around 2017 and was
quickly forgotten.)

.. _FreeCAD: https://www.freecad.org/



Details
=======

A `6" x 9" x ⅛" borosilicate glass plate from McMaster-Carr
<https://www.mcmaster.com/8476K74-8476K741/>`_ was used.

The retainers fit over the bed assembly and abut the glass plate,
holding it in place.  The retainers are secured using the corner bed
screws.

.. figure:: doc/images/KnP3D_glass_bed_retainer-closeup.jpeg
   :alt: Close-up photo of retainer assembly installed on print bed.

   Retainer assembly installed on print bed.

The OE bed M3-0.5 screws on the KnP3D are too short to accomodate the
retainers.  You'll need screws about 20mm in length (at most).
`McMaster-Carr #91420A128 <https://www.mcmaster.com/91420A128/>`_ is a
suitable replacement.

Heat up the bed of your printer to working temperature before adjusting
the retainers and tightening the nuts.  Check tightness periodically
throughout long prints.



Future Plans
============

None at this time.  The design is not perfect, but works well enough,
and is as simple as it gets.



Copying
=======

Everything here is copyright © 2023 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
